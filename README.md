﻿# 1. High Level Voice Service Core (VSHL-CORE)
This repository hosts the code for the AGL's high level voice service's core request arbitration binding also known as VSHL-CORE.
Please refer to the [architecture](https://confluence.automotivelinux.org/display/SPE/Speech+EG+Architecture) for more information.

# 2. Build Dependencies and License Information

During the build time, the following dependencies are fetched and run by the build system. Please refer to each of the individual entities for the particular licenses.
* [Google Test v1.8.0](https://github.com/google/googletest) when compiled with ENABLE_UNIT_TESTS option.

# 3. Getting the Source Code
```
export MY_PROJECTS_DIR = <Your Project Directory>
pushd $MY_PROJECTS_DIR
git clone --recursive https://gerrit.automotivelinux.org/gerrit/apps/agl-service-voice-high-core
```

# 4. Renesas R-Car M3/H3 board
## 4.1 Building VSHL Core

```
pushd agl-service-voice-high-core
mkdir build
pushd build
source /opt/agl-sdk/6.0.1-aarch64/environment-setup-aarch64-agl-linux
cmake ..
make autobuild
popd
./conf.d/autobuild/agl/autobuild package
```
* The build output will be located at $MY_PROJECTS_DIR/agl-service-voice-high-core/build/vshl-core.wgt

## 4.2 Running VSHL Core
```
# afm-util install vshl-core.wgt
# afm-util start vshl-core@1.0
```
## 4.3 Voiceagent Configuration
The binding's voiceagent configuration can be provided in three ways.  Via the binding's app controller JSON configuration, the file /etc/xdg/AGL/voice-high.json, or as separate per-voiceagent JSON files in /etc/xdg/AGL/voiceagents/.  These will be discussed in the following sections.

### 4.3.1 App Controller Voiceagent Configuration
The original method of providing per-voiceagent configuration and setting the default voiceagent was via the binding's app-controller JSON configuration, which is located in conf.d/project/etc/vshl-core-api.json in the source tree. This configuration file defines the runtime configuration of the binding used by the app-controller library, and voiceagent configuration was, and still can be, provided via the "args" parameter for the controller's "onload" definition for the "loadVoiceAgentsConfig" entry point in the vshl-core plugin.  The original hard-coded configuration for the Alexa voiceagent looked like this:
```
"onload": [{
  "uid": "loadVoiceAgentsConfig",
  "info": "Loading the information about voice agents managed by the high level voice service.",
  "action": "plugin://vshl-core#loadVoiceAgentsConfig",
  "args": {
    "default": "VA-001",
    "agents": [
      {
        "id": "VA-001",
        "active": true,
        "name": "Alexa",
        "api": "alexa-voiceagent",
        "wakewords": [
          "alexa",
          "computer",
          "echo"
        ],
        "activewakeword": "alexa",
        "description": "Alexa voice assistant by Amazon.",
        "vendor": "Amazon.com Services Inc"
      }
    ]
  }
}],
```
The "args" definition has now been removed in favor of the flexibility provided by the following options, but can still be added back if required for a future use case.

### 4.3.2 /etc/xdg/AGL/voice-high.json
The file /etc/xdg/AGL/voice-high.json can be considered a replacement for the configuration previously provided in the "args" to "loadVoiceAgentsConfig" in the controller configuration.  Its syntax is the same, so the following voice-high.json contents will produce the same final binding configuration:
```
{
  "default": "VA-001",
  "agents": [
    {
      "id": "VA-001",
      "active": true,
      "name": "Alexa",
      "api": "alexa-voiceagent",
      "wakewords": [
        "alexa",
        "computer",
        "echo"
      ],
      "activewakeword": "alexa",
      "description": "Alexa voice assistant by Amazon.",
      "vendor": "Amazon.com Services Inc"
    }
  ]
}
```
The configuration parsing, however, allows omitting the list of voiceagents, so in practice the per-voiceagent configuration files described next would preferably be used with a simpler voice-high.json file that just defines the default voiceagent, e.g.:
```
{ "default": "VA-001" }
```
Note that the binding does allow operation with no voiceagents as well as no default voiceagent configured. As well, since the voiceagent API specified by name in the "api" field is loaded dynamically, there currently is a constraint that the providing voiceagent binding be running before vshl-core.

### 4.3.3 /etc/xdg/AGL/voiceagents
The final option for providing just per-voiceagent configuration is to place a JSON file containing its configuration in the directory /etc/xdg/AGL/voiceagents.  All readable files in the directory with the .json extension will be read and validated as possible voiceagent configurations.  The contents are just the "agents" field of the previous configurations, so for the Alexa voiceagent example above, the JSON would be:
```
{
  "id": "VA-001",
  "active": true,
  "name": "Alexa",
  "api": "alexa-voiceagent",
  "wakewords": [
    "alexa",
    "computer",
    "echo"
  ],
  "activewakeword": "alexa",
  "description": "Alexa voice assistant by Amazon.",
  "vendor": "Amazon.com Services Inc"
}
```
Note that for conflicting voiceagent definitions with the same "id", the first configuration parsed will be used, and the parsing ordering follows what has been outlined above, i.e. controller JSON configuration, then voice-high.json, then any per-voiceagent configuration JSON files. However, the value of the default voiceagent, if any, provided by the controller JSON configuration, may be over-ridden by the value in voice-high.json.

# 5. Ubuntu 16.04
## 5.1 Building VSHL Core

```
pushd agl-service-voice-high-core
mkdir build
pushd build
cmake ..
make autobuild
popd
./conf.d/autobuild/linux/autobuild package
```
To build the included unit tests modify the cmake step as following:
cmake .. -DENABLE_UNIT_TESTS=ON

## 5.2 Running VSHL Core
```
afb-daemon --port=1111 --name=afb-vshl-core --workdir=$MY_PROJECTS_DIR/agl-voice-service-high-core/build/package --ldpaths=lib --roothttp=htdocs --token= -vvv
```

# 6. Running the Unit Tests
## 6.1 Ubuntu 16.04
```
pushd agl-service-voice-high-core/
./build/src/plugins/vshl-core-api_Test
popd
```

# 7. Testing VSHL Core
* The binding can be tested by launching the HTML5 sample application that is bundled with the package in a browser.

```
http://localhost:1111
```

# 8. Contributing code
Before contributing the source, its recommended to format the code with clang-format. This is done automatically during commit step if following instructions are followed.
**Prerequisite**: Install clang-format-6.0 or greater.
There are following 2 options.

* Before commit, manually run clang-format on the file (It will use local .clang-format file for checking the rules)
```
clang-format -i <path to source file>
```

* Setup clang-format as pre-commit git hook. This is one time step after you clone the repo
```
cd ${VSHL_CORE_ROOT}
cp tools/pre-commit .git/hooks/pre-commit
```

* With the hook in place, everytime you try to commit, it will check the format and disallow commit if format doesn't abide by the .clang-format rules.
It will also give you the option to apply a patch (it creates a patch in /tmp folder) to make the source abide by the .clang-format rules. Apply the patch and proceed to commit
```
git apply /tmp/<patch>
git add <source files>
git commit
```
