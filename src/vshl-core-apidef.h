
static const char _afb_description_vshl_core[] =
    "{\"openapi\":\"3.0.0\",\"$schema\":\"http://iot.bzh/download/openapi/sch"
    "ema-3.0/default-schema.json\",\"info\":{\"description\":\"\",\"title\":\""
    "High Level Voice Service Core API\",\"version\":\"1.0\",\"x-binding-c-ge"
    "nerator\":{\"api\":\"vshl-core\",\"version\":3,\"prefix\":\"afv_\",\"pos"
    "tfix\":\"\",\"start\":null,\"onevent\":null,\"init\":\"init\",\"scope\":"
    "\"\",\"private\":false,\"noconcurrency\":true}},\"servers\":[{\"url\":\""
    "ws://{host}:{port}/api/monitor\",\"description\":\"TS caching binding\","
    "\"variables\":{\"host\":{\"default\":\"localhost\"},\"port\":{\"default\""
    ":\"1234\"}},\"x-afb-events\":[{\"$ref\":\"#/components/schemas/afb-event"
    "\"}]}],\"components\":{\"schemas\":{\"afb-reply\":{\"$ref\":\"#/componen"
    "ts/schemas/afb-reply-v3\"},\"afb-event\":{\"$ref\":\"#/components/schema"
    "s/afb-event-v3\"},\"afb-reply-v3\":{\"title\":\"Generic response.\",\"ty"
    "pe\":\"object\",\"required\":[\"jtype\",\"request\"],\"properties\":{\"j"
    "type\":{\"type\":\"string\",\"const\":\"afb-reply\"},\"request\":{\"type"
    "\":\"object\",\"required\":[\"status\"],\"properties\":{\"status\":{\"ty"
    "pe\":\"string\"},\"info\":{\"type\":\"string\"},\"token\":{\"type\":\"st"
    "ring\"},\"uuid\":{\"type\":\"string\"},\"reqid\":{\"type\":\"string\"}}}"
    ",\"response\":{\"type\":\"object\"}}},\"afb-event-v3\":{\"type\":\"objec"
    "t\",\"required\":[\"jtype\",\"event\"],\"properties\":{\"jtype\":{\"type"
    "\":\"string\",\"const\":\"afb-event\"},\"event\":{\"type\":\"string\"},\""
    "data\":{\"type\":\"object\"}}}},\"responses\":{\"200\":{\"description\":"
    "\"A complex object array response\",\"content\":{\"application/json\":{\""
    "schema\":{\"$ref\":\"#/components/schemas/afb-reply\"}}}}}}}"
;


static const struct afb_verb_v3 _afb_verbs_vshl_core[] = {
    {
        .verb = NULL,
        .callback = NULL,
        .auth = NULL,
        .info = NULL,
        .session = 0,
        .vcbdata = NULL,
        .glob = 0
	}
};

const struct afb_binding_v3 afbBindingV3 = {
    .api = "vshl-core",
    .specification = _afb_description_vshl_core,
    .info = "",
    .verbs = _afb_verbs_vshl_core,
    .preinit = NULL,
    .init = init,
    .onevent = NULL,
    .userdata = NULL,
    .noconcurrency = 1
};

